package com.rafaelalves.workshopmongo.dto;

import java.io.Serializable;

import com.rafaelalves.workshopmongo.models.User;

import lombok.Data;


@Data
public class AuthorDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String id;
    private String nome;
    
    public AuthorDTO(){}

    public AuthorDTO(User user) {
        this.id = user.getId();
        this.nome = user.getNome();
    }
}
