package com.rafaelalves.workshopmongo.repositories;

import java.util.Date;
import java.util.List;

import com.rafaelalves.workshopmongo.models.Post;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends MongoRepository<Post, String> {
    
    @Query("{'titulo':{$regex : ?0, $options: 'i'}}")
    List<Post> buscaTitulo(String text);
    
    List<Post> findByTituloContainingIgnoreCase(String text);

    @Query("{ $and: [ { data: {$gte: ?1} }, { data: { $lte: ?2} } , { $or: [ { 'titulo': { $regex: ?0, $options: 'i' } }, { 'conteudo': { $regex: ?0, $options: 'i' } }, { 'comentarios.text': { $regex: ?0, $options: 'i' } } ] } ] }")
	List<Post> buscaGeral(String text, Date minDate, Date maxDate);
}
