package com.rafaelalves.workshopmongo.services;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.rafaelalves.workshopmongo.models.Post;
import com.rafaelalves.workshopmongo.repositories.PostRepository;
import com.rafaelalves.workshopmongo.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    public Post findById(String id) {
        Optional<Post> obj = postRepository.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
    }

    public List<Post> findByTitulo(String text){
        return postRepository.buscaTitulo(text);
    }

    public List<Post> buscaGeral(String text, Date minDate, Date maxDate){
        maxDate = new Date(maxDate.getTime() + 24 * 60 * 60 * 1000);
        return postRepository.buscaGeral(text, minDate, maxDate);
    }

}
