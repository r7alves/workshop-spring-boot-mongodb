package com.rafaelalves.workshopmongo.services;

import java.util.List;
import java.util.Optional;

import com.rafaelalves.workshopmongo.dto.UserDTO;
import com.rafaelalves.workshopmongo.models.User;
import com.rafaelalves.workshopmongo.repositories.UserRepository;
import com.rafaelalves.workshopmongo.services.exceptions.ObjectNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findById(String id) {
        Optional<User> obj = userRepository.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado"));
    }

    public User store(User user){
        if (user.getId() == null) {
            user.setId(null);
        } else {
            findById(user.getId());
        }

        return userRepository.save(user);    
    }

    public void deleta(String id){
        findById(id);
        userRepository.deleteById(id);
    }

    public User fromDTO(UserDTO userDTO){
        
        return new User(userDTO.getId(), userDTO.getNome(), userDTO.getEmail());
    }
}
