package com.rafaelalves.workshopmongo.resources;

import java.util.Date;
import java.util.List;

// import java.util.ArrayList;
// import java.util.Arrays;

// import javax.websocket.server.PathParam;

import com.rafaelalves.workshopmongo.models.Post;
import com.rafaelalves.workshopmongo.resources.utils.URL;
import com.rafaelalves.workshopmongo.services.PostService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/posts")
public class PostResource {

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<Post> findById(@PathVariable String id) {

        Post post = postService.findById(id);

        return ResponseEntity.ok().body(post);

    }
    
    @RequestMapping(value = "/busca", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> findByTitulo(@RequestParam(value = "text", defaultValue = "") String text) {

        text = URL.decodeParam(text);

        List<Post> posts = postService.findByTitulo(text);

        return ResponseEntity.ok().body(posts);

    }
    
    @RequestMapping(value = "/busca-geral", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> buscaGeral(
        @RequestParam(value = "text", defaultValue = "") String text,
        @RequestParam(value = "min-date", defaultValue = "") String minDate,
        @RequestParam(value = "max-date", defaultValue = "") String maxDate
        
    ) {

        Date min = URL.converteData(minDate, new Date(0L));
        Date max = URL.converteData(maxDate, new Date());

        text = URL.decodeParam(text);

        List<Post> posts = postService.buscaGeral(text, min, max);

        return ResponseEntity.ok().body(posts);

    }


    
}
