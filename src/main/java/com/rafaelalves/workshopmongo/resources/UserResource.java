package com.rafaelalves.workshopmongo.resources;

import java.net.URI;
// import java.util.ArrayList;
// import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

// import javax.websocket.server.PathParam;

import com.rafaelalves.workshopmongo.dto.UserDTO;
import com.rafaelalves.workshopmongo.models.Post;
import com.rafaelalves.workshopmongo.models.User;
import com.rafaelalves.workshopmongo.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@Controller
@RequestMapping(value = "/users")
public class UserResource {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<UserDTO>> findAll() {

        List<User> list = userService.findAll();

        List<UserDTO> listDto = list.stream().map(user -> new UserDTO(user)).collect(Collectors.toList());

        return ResponseEntity.ok().body(listDto);

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<UserDTO> findById(@PathVariable String id) {

        User user = userService.findById(id);

        return ResponseEntity.ok().body(new UserDTO(user));

    }
        
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<User> store(@RequestBody UserDTO userDTO){
        
        User user = userService.fromDTO(userDTO);

        user = userService.store(user);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(user.getId()).toUri();

        return ResponseEntity.created(uri).body(user);
        //return ResponseEntity.created(uri).build();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Void> update(@RequestBody UserDTO userDTO, @PathVariable String id )  {
        User user = userService.fromDTO(userDTO);
        
        user.setId(id);
        user = userService.store(user);

        return ResponseEntity.noContent().build();

    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> delete(@PathVariable String id) {

        userService.deleta(id);

        return ResponseEntity.noContent().build();

    }

    @RequestMapping(value = "/{id}/posts", method = RequestMethod.GET)
    public ResponseEntity<List<Post>> posts(@PathVariable String id) {

        User user = userService.findById(id);

        return ResponseEntity.ok().body( user.getPosts());

    }
}
