package com.rafaelalves.workshopmongo.config;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.TimeZone;

import com.rafaelalves.workshopmongo.dto.AuthorDTO;
import com.rafaelalves.workshopmongo.dto.CommentDTO;
import com.rafaelalves.workshopmongo.models.Post;
import com.rafaelalves.workshopmongo.models.User;
import com.rafaelalves.workshopmongo.repositories.PostRepository;
import com.rafaelalves.workshopmongo.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Instantiation implements CommandLineRunner{

    @Autowired
    private PostRepository postRepository;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        
        userRepository.deleteAll();
        postRepository.deleteAll();

        User maria = new User(null, "Maria Brown", "maria@gmail.com");
        User alex = new User(null, "Alex Green", "alex@gmail.com");
        User bob = new User(null, "Bob Grey", "bob@gmail.com");

        userRepository.saveAll(Arrays.asList(maria, bob, alex));

        Post post1 = new Post(null, sdf.parse("21/11/2020"), "Partiu Viagem", "Vou viajar ....", new AuthorDTO(maria));
        Post post2 = new Post(null, sdf.parse("01/12/2020"), "Bom dia", "Vai te f ...", new AuthorDTO(maria));

        CommentDTO c1 = new CommentDTO("Boa viagem", sdf.parse("21/11/2020"), new AuthorDTO(alex));
        CommentDTO c2 = new CommentDTO("Vai timbora", sdf.parse("21/11/2020"), new AuthorDTO(bob));
        CommentDTO c3 = new CommentDTO("Sai diab...", sdf.parse("01/12/2020"), new AuthorDTO(alex));

        post1.getComentarios().addAll(Arrays.asList(c1,c2));
        post2.getComentarios().addAll(Arrays.asList(c3));
        
        postRepository.saveAll(Arrays.asList(post1, post2));

        maria.getPosts().addAll(Arrays.asList(post1,post2));
        userRepository.save(maria);
    }
    
}
