package com.rafaelalves.workshopmongo.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.rafaelalves.workshopmongo.dto.AuthorDTO;
import com.rafaelalves.workshopmongo.dto.CommentDTO;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "posts")
public class Post  implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    private String id;
    private Date data;
    private String titulo;
    private String conteudo;
    private AuthorDTO author;

    private List<CommentDTO> comentarios = new ArrayList<>();

    public Post(){

    }

    public Post(String id, Date data, String titulo, String conteudo, AuthorDTO author) {
        this.id = id;
        this.data = data;
        this.titulo = titulo;
        this.conteudo = conteudo;
        this.author = author;
    }
        
}
